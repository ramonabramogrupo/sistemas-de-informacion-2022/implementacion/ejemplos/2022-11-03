﻿/*
  CONSULTAS DE ACCION
 
 */

USE compranp;

-- CONSULTA 1
-- COLOCAR LOS TOTALES A 0 EN COMPRANP

UPDATE compranp c
  SET c.total=0;

SELECT * FROM compranp c;

-- CONSULTA 2
-- COLOCAR LOS TOTALES A NULL EN COMPRANP PERO SOLAMENTE
-- SI ESTAN A 0


UPDATE compranp c
  SET c.total=NULL
  WHERE c.total=0;

UPDATE (SELECT * FROM compranp) c1
  SET c1.total=0;


-- CONSULTA 3
-- AÑADIR UN CAMPO A LA TABLA compran DENOMINADO numeroClientes 
-- QUE ME INDIQUE EL NUMERO DE CLIENTES QUE HAN COMPRADO ESE PRODUCTO

-- añado el campo
ALTER TABLE compran ADD COLUMN numeroClientes int DEFAULT 0;

-- comprobar que el campo se ha añadido
SHOW FIELDS FROM compran;
EXPLAIN compran;
DESCRIBE compran;

-- c1
-- consulta de seleccion   
                                                                            
SELECT 
    c.codPro,COUNT(*) numero 
  FROM compran c 
  GROUP BY c.codPro;

SELECT * FROM compran c JOIN 
  (
    SELECT 
      c.codPro,COUNT(*) numero 
    FROM compran c 
    GROUP BY c.codPro
  ) c1 USING(codPro);

-- consulta de accion
UPDATE compran c JOIN
  (
     SELECT 
      c.codPro,COUNT(*) numero 
    FROM compran c 
    GROUP BY c.codPro
  ) c1 USING(codPro)
  SET c.numeroClientes=c1.numero;

-- comprobar el resultado
SELECT * FROM compran;     


-- CONSULTA 4
-- ACTUALIZAR EL CAMPO numeroClientes DE LA TABLA PRODUCTOS 
-- PARA QUE ME INDIQUE EL NUMERO DE CLIENTES QUE HAN COMPRADO ESE PRODUCTO
-- VOLVER A EJERCUTAR EL SCRIPT ORIGINAL COMPRANP

-- C1
-- CONSULTA DE SELECCION
 SELECT 
    c.codPro,COUNT(*) numero 
  FROM 
    compran c
  GROUP BY c.codPro;

-- consulta de actualizacion

  UPDATE 
      productos p 
    JOIN
      (
        SELECT 
          c.codPro,COUNT(*) numero 
        FROM 
          compran c
        GROUP BY c.codPro
      ) c1 USING(codPro)
    SET p.numeroClientes=c1.numero;